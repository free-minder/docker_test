o2server_demo cookbooks
=======================

These cookbooks configures host server for docker service with local registry server and docker image with o2server.
------------------------------------------------------------------------------------------------------------------------------------------------------------

There are 2 files missing(gitignored). You have to create them and fill with your AWS credentials in this format:

/cookbooks/docker_cfg/files/default/aws_config
```
#!python
[default]
aws_access_key_id = YOUR_ACCESS_KEY_ID_FOR_S3
aws_secret_access_key = YOUR_SECRET_ACCESS_KEY_FOR_S3

```

/cookbooks/docker_cfg/files/default/o2healthcheck_ses.rb
```
#!ruby
#!/usr/bin/env ruby
require 'aws/ses'
ses = AWS::SES::Base.new(
  :access_key_id     => 'YOUR_ACCESS_KEY_ID_FOR_SES', 
  :secret_access_key => 'YOUR_SECRET_ACCESS_KEY_FOR_SES',
  :server => 'email.us-west-2.amazonaws.com'
)
```