#!/usr/bin/env ruby
require_relative 'o2healthcheck_ses'
o2get = `curl -s -o /dev/null -w "%{http_code}" http://localhost:8001/healthcheck`

# one time loop
while o2get != "200" do
	# wait 20 seconds for o2server startup
	sleep 20
	`echo "#{Time.now} Something wrong with o2server! Error code is #{o2get}." >> /var/log/o2healthcheck.log`
end

ses.send_email(
	:to        => ['dmitry.zhukov@gmail.com'],
	:source    => '"Dmitry Zhukov" <dmitry.zhukov@gmail.com>',
	:subject   => 'o2server started',
	:text_body => 'Server status is OK.'
) if o2get == "200"
`echo "#{Time.now} Server status is OK" >> /var/log/o2healthcheck.log` if o2get == "200"

# forever loop
loop do
	sleep 30
	`echo "#{Time.now} Server status is OK" >> /var/log/o2healthcheck.log` if o2get == "200"
ses.send_email(
	:to        => ['dmitry.zhukov@gmail.com'],
	:source    => '"Dmitry Zhukov" <dmitry.zhukov@gmail.com>',
	:subject   => 'o2server failed',
	:text_body => 'Something wrong with o2server!'
) if o2get != "200"
	`echo "#{Time.now} Something wrong with o2server! Error code is #{o2get}." >> /var/log/o2healthcheck.log` if o2get != "200"
end
