# INSTALL AND DEFINE SERVICES
package 'docker'
package 'htop'
package 'mc'
package 'wget'
package 'curl'
package 'git'
package 'sysv-rc-conf' if node['platform_family'] == 'debian'


# PATCH /etc/inputrc
cookbook_file '/etc/inputrc' do
   mode '0644'
   owner 'root'
   group 'root'
end

# Midnight Commander dark theme
directory '/root/.local/share/mc/skins' do
	mode '0755'
	owner 'root'
	group 'root'
	action :create
	recursive true
end

cookbook_file '/root/.local/share/mc/skins/ajnasz-blue.ini' do
	mode '0640'
	owner 'root'
	group 'root'
	source 'ajnasz-blue.ini'
end

# Remote agent for Sublime Text
execute "Download and make executable rsub" do
	user "root"
	command "wget -O /usr/local/bin/rsub https://raw.github.com/aurora/rmate/master/rmate && chmod +x /usr/local/bin/rsub"
	not_if { ::File.exist?('/usr/local/bin/rsub') }
end

# Healthcheck scripts
cookbook_file '/usr/local/sbin/o2healthcheck.rb' do
	mode '0755'
	owner 'root'
	group 'root'
	source 'o2healthcheck.rb'
end
cookbook_file '/usr/local/sbin/o2healthcheck_ses.rb' do
	mode '0640'
	owner 'root'
	group 'root'
	source 'o2healthcheck_ses.rb'
end

#################################################################
# Automation of docker install and configuration.
service 'docker' do
  supports :reload => true
  action :start
end

execute "docker pull and run registry" do
	command "docker run -p 5000:5000 -d registry"
	user "root"
end

execute "docker download oracle jre 8 image" do
	command "docker pull jamesdbloom/docker-java8-maven"
	user "root"
	only_if { File.exist?("/var/lib/docker/repositories-devicemapper") }
end


# Create temp dir for files
directory '/tmp/o2server_demo' do
	mode '0755'
	owner 'root'
	group 'root'
	action :create
	recursive true
end

cookbook_file '/tmp/o2server_demo/inputrc' do
	mode '0640'
	owner 'root'
	group 'root'
end

cookbook_file '/tmp/o2server_demo/ajnasz-blue.ini' do
	mode '0640'
	owner 'root'
	group 'root'
end

cookbook_file '/tmp/o2server_demo/aws_config' do
	mode '0640'
	owner 'root'
	group 'root'
	source 'aws_config'
end

cookbook_file '/tmp/o2server_demo/Dockerfile' do
	mode '0640'
	owner 'root'
	group 'root'
	source 'Dockerfile'
end

execute "build customized docker image" do
	command "docker build -t docker_o2server_demo:v1 /tmp/o2server_demo"
	user "root"
	only_if { File.exist?("/var/lib/docker/repositories-devicemapper") }
end

execute "run container with o2server" do
	command "docker run -p 8000:8000 -p 8001:8001 -d docker_o2server_demo:v1"
	user "root"
	only_if { File.exist?("/var/lib/docker/repositories-devicemapper") }
end

execute "install AWS SES gem for o2healthcheck notifications" do
	command "gem install aws-ses"
	user "root"
end

execute "run healthcheck" do
	command "/usr/bin/env ruby /usr/local/sbin/o2healthcheck.rb 2>&1 &"
	user "root"
end

execute "remove dir with temp files" do
	command "rm -fr /tmp/o2server_demo"
	user "root"
end

# save docker image to local registry server
execute "save docker image to local registry server" do
	command "docker tag docker_o2server_demo:v1 localhost:5000/docker_o2server_demo:v1 && docker push localhost:5000/docker_o2server_demo:v1"
	user "root"
	only_if { File.exist?("/var/lib/docker/repositories-devicemapper") }
end

